# Desafio back-end

Para rodar localmente basta executar o comando abaixo e a função main da classe InfogloboBackendApplication.

```
mvn clean package
```
O servidor será iniciado em [http://localhost:8080/](http://localhost:8080/).

Para visualizar o feed é necessário realizar login no servidor em [http://localhost:8080/login](http://localhost:8080/login), 
utilizando os dados abaixo.

```
username : user
password : password
```

Também é possível utilizar Docker com o comando abaixo.

```
docker-compose up -d
```
package br.com.igbeni.infoglobobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfogloboBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(InfogloboBackendApplication.class, args);
    }
}

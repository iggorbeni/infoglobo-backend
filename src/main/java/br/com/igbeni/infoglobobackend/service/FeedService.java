package br.com.igbeni.infoglobobackend.service;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.jdom.Document;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.List;

@Service
public class FeedService {
    private static final Logger logger = LoggerFactory.getLogger(FeedService.class);


    private static final String FEED_URL = "http://revistaautoesporte.globo.com/rss/ultimas/feed.xml";

    protected Document getDocument() {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(FEED_URL);

        HttpResponse response;
        Document document = null;
        try {
            response = client.execute(request);

            logger.info("Response Code : {}", response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuilder result = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            logger.info("Feed as String : {}", result.toString());

            SAXBuilder builder = new SAXBuilder();

            try {
                document = builder.build(new StringReader(result.toString()));
            } catch (JDOMException | IOException e) {
                logger.error(e.getMessage(), e);
                e.printStackTrace();
            }

            logger.info("Feed as Document : {}", document);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }

        return document;
    }

    private void parseItems(JSONArray itemJsonArray, List items) {
        for (Object item : items) {
            if (item instanceof Element) {
                Element element = (Element) item;

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("title", element.getChild("title").getValue());
                jsonObject.put("link", element.getChild("link").getValue());

                org.jsoup.nodes.Element description = Jsoup.parse(element.getChild("description").getValue()).body();

                JSONArray jsonArray = new JSONArray();

                for (org.jsoup.nodes.Element pElement : description.select("> p:not(:empty)")) {
                    if (!pElement.text().isEmpty() && !pElement.text().equalsIgnoreCase(" ")) {
                        insertTypeTextIntoJsonArray(jsonArray, pElement.text());
                    }
                }

                for (org.jsoup.nodes.Element imgElement : description.select("> div > img")) {
                    if (!imgElement.attr("src").isEmpty()) {
                       insertTypeImageIntoJsonArray(jsonArray, imgElement.attr("src"));
                    }
                }

                Elements aElements = description.select("> div > ul > li > a");
                if (aElements.size() > 0) {
                    JSONArray linksJsonArray = new JSONArray();
                    for (org.jsoup.nodes.Element aElement : aElements) {
                        if (!aElement.attr("href").isEmpty()) {
                            linksJsonArray.put(aElement.attr("href"));
                        }
                    }
                    insertTypeLinksIntoJsonArray(jsonArray, linksJsonArray);
                }

                jsonObject.put("description", jsonArray);

                JSONObject itemObject = new JSONObject();
                itemObject.put("item", jsonObject);
                itemJsonArray.put(itemObject);
            }
        }
    }

    protected void insertTypeIntoJsonArray(JSONArray deionJsonArray, Object obj, String type) throws InvalidObjectException {
        Object content = null;

        if (obj instanceof String) {
            String text = (String) obj;

            if (text.length() > 0) {
                content = text;
            }
        } else if (obj instanceof JSONArray) {
            content = (JSONArray) obj;
        } else {
            throw new InvalidObjectException("Invalid input object.");
        }

        if (content != null) {
            JSONObject linksJsonObject = new JSONObject();
            linksJsonObject.put("type", type);
            linksJsonObject.put("content", content);
            deionJsonArray.put(linksJsonObject);
        }
    }

    protected void insertTypeLinksIntoJsonArray(JSONArray deionJsonArray, JSONArray jsonArray) {
        try {
            insertTypeIntoJsonArray(deionJsonArray, jsonArray, "links");
        } catch (InvalidObjectException e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
    }

    protected void insertTypeImageIntoJsonArray(JSONArray deionJsonArray, String src) {
        try {
            insertTypeIntoJsonArray(deionJsonArray, src, "image");
        } catch (InvalidObjectException e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
    }

    protected void insertTypeTextIntoJsonArray(JSONArray deionJsonArray, String text) {
        try {
            insertTypeIntoJsonArray(deionJsonArray, text, "text");
        } catch (InvalidObjectException e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
    }

    public JSONObject getFeed() {
        Document document = getDocument();

        if (document == null) {
            return null;
        }

        Element rootNode = document.getRootElement();
        List items = rootNode.getChild("channel").getChildren("item");

        JSONArray itemJsonArray = new JSONArray();
        parseItems(itemJsonArray, items);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("feed", itemJsonArray);

        return jsonObject;
    }
}

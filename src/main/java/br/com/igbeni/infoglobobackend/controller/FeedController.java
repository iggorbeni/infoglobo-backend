package br.com.igbeni.infoglobobackend.controller;

import br.com.igbeni.infoglobobackend.service.FeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "feed")
public class FeedController {

    @Autowired
    private FeedService feedService;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getFeed() {
        return ResponseEntity.ok(feedService.getFeed().toString());
    }
}
